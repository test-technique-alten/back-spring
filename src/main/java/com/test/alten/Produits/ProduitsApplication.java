package com.test.alten.Produits;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.alten.Produits.dao.ProductRepository;
import com.test.alten.Produits.model.Product;

@SpringBootApplication
public class ProduitsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProduitsApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(ProductRepository productRepository) {
		return args -> {
			if (productRepository.count() == 0) {
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Product>> typeReference = new TypeReference<List<Product>>() {
			};
			InputStream inputStream = TypeReference.class.getResourceAsStream("/json/products.json");
			try {
				List<Product> products = mapper.readValue(inputStream, typeReference);
				productRepository.saveAll(products);
				System.out.println("***** Liste des produits importée depuis le fichier JSON *****");
			} catch (IOException e) {
				System.out.println("***** Echec de l'import depuis le fichier JSON *****" + e.getMessage());
			}
			} else {
				System.out.println("***** La base de donnée n'est pas vide, le fichier JSON ne sera pas importé *****");
			}

		};
	}

}
