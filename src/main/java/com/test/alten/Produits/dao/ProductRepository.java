package com.test.alten.Produits.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import com.test.alten.Produits.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {


}
