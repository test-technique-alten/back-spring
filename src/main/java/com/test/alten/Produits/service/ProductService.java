package com.test.alten.Produits.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.test.alten.Produits.dao.ProductRepository;
import com.test.alten.Produits.model.Product;

@Service
public class ProductService {
	
	private ProductRepository productRepository;
	
	public ProductService(ProductRepository productRepository) {
		this.productRepository = productRepository;
		
	}
	
	public Iterable<Product> list(){
		return productRepository.findAll();
		}
	
	public Iterable<Product> save(List<Product> products){
		return productRepository.saveAll(products);
	}
	
}
