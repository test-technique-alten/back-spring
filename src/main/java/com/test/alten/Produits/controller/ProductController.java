package com.test.alten.Produits.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.alten.Produits.dao.ProductRepository;
import com.test.alten.Produits.model.Product;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	ProductRepository productRepository;

	@PostMapping(path = "/create")
	public ResponseEntity<Product> createProduct(@RequestBody Product product) {
		try {
			Product _product = new Product(product.getCode(), product.getName(), product.getDescription(),
					product.getPrice(), product.getQuantity(), product.getInventoryStatus(), product.getCategory(), product.getImage(), product.getRating());
			productRepository.save(_product);
			return new ResponseEntity<>(_product, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/get")
	public ResponseEntity<List<Product>> getProducts() {
		List<Product> products = productRepository.findAll();
		if (products.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(products, HttpStatus.OK);
		}

	}

	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable("id") Long productId) {
		Optional<Product> _product = productRepository.findById(productId);
		if (_product.isPresent()) {
			return new ResponseEntity<>(_product.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

	}

	@PatchMapping(path = "/update/{id}")
	public ResponseEntity<Product> updateProduct(@PathVariable("id") Long productId, @RequestBody Product product) {
		Optional<Product> _productToUpdate = productRepository.findById(productId);
		if (_productToUpdate.isPresent()) {
			Product _product = _productToUpdate.get();
	        if (product.getCode() != null) {
	            _product.setCode(product.getCode());
	        }
	        if (product.getName() != null) {
	            _product.setName(product.getName());
	        }
	        if (product.getDescription() != null) {
	            _product.setDescription(product.getDescription());
	        }
	        if (product.getPrice() != null) {
	            _product.setPrice(product.getPrice());
	        }
	        if (product.getQuantity() != null) {
	            _product.setQuantity(product.getQuantity());
	        }
	        if (product.getInventoryStatus() != null) {
	            _product.setInventoryStatus(product.getInventoryStatus());
	        }
	        if (product.getCategory() != null) {
	            _product.setCategory(product.getCategory());
	        }
	        if (product.getImage() != null) {
	            _product.setImage(product.getImage());
	        }
	        if (product.getRating() != null) {
	            _product.setRating(product.getRating());
	        }
			return new ResponseEntity<>(productRepository.save(_product), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<Product> deleteProduct(@PathVariable("id") Long productId) {
		try {
			productRepository.deleteById(productId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
